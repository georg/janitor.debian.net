all: docker-schedule docker-worker docker-postgres k8s schedule/upstream_project_pb2.py docker-site

DOCKER_SALSA_NS = registry.salsa.debian.org/jelmer/janitor.debian.net/
DOCKER_GCR_NS = eu.gcr.io/debian-janitor/

CRUNCHY_VERSION = centos8-13.5-4.7.4

docker-all: docker-tags docker-worker docker-postgres docker-site

docker-tags: docker-tag-crunchy-pgbackrest docker-tag-crunchy-upgrade docker-tag-crunchy-pgbadger docker-tag-crunchy-pgbouncer docker-tag-crunchy-pgbackrest-repo # docker-tag-pgo-rmdata

docker-tag-%:
	docker pull registry.developers.crunchydata.com/crunchydata/$*:$(CRUNCHY_VERSION)
	docker tag registry.developers.crunchydata.com/crunchydata/$*:$(CRUNCHY_VERSION) $(DOCKER_SALSA_NS)$*:latest
	docker tag registry.developers.crunchydata.com/crunchydata/$*:$(CRUNCHY_VERSION) $(DOCKER_GCR_NS)$*:latest
	docker push $(DOCKER_SALSA_NS)$*:latest
	docker push $(DOCKER_GCR_NS)$*:latest

docker-%: Dockerfile_%
	docker build -f $< . -t $(DOCKER_SALSA_NS)$* -t $(DOCKER_GCR_NS)$*
	docker push $(DOCKER_SALSA_NS)$*
	docker push $(DOCKER_GCR_NS)$*

docker-force-%: Dockerfile_%
	docker build -f $< . -t $(DOCKER_SALSA_NS)$*  -t $(DOCKER_GCR_NS)$* --no-cache
	docker push $(DOCKER_SALSA_NS)$*
	docker push $(DOCKER_GCR_NS)$*

docker-postgres: Dockerfile_postgres
	docker build -f $< . -t $(DOCKER_SALSA_NS)postgres -t $(DOCKER_GCR_NS)postgres --build-arg=FROM_IMAGE=crunchy-postgres --build-arg=CRUNCHY_VERSION=centos8-13.5-0
	docker push $(DOCKER_SALSA_NS)postgres
	docker push $(DOCKER_GCR_NS)postgres

docker-postgres-ha: Dockerfile_postgres
	docker build -f $< . -t $(DOCKER_SALSA_NS)postgres-ha -t $(DOCKER_GCR_NS)postgres-ha --build-arg=FROM_IMAGE=crunchy-postgres-ha --build-arg=CRUNCHY_VERSION=centos8-13.5-0
	docker push $(DOCKER_SALSA_NS)postgres-ha
	docker push $(DOCKER_GCR_NS)postgres-ha

deploy-worker:
	./update-workers.sh

deploy-site:
	./k8s/kubectl rollout restart deploy site

deploy-runner:
	./k8s/kubectl rollout restart deploy runner

deploy-vcs-store:
	./k8s/kubectl rollout restart deploy vcs-store

deploy-archive:
	./k8s/kubectl rollout restart statefulset archive

deploy-differ:
	./k8s/kubectl rollout restart statefulset differ

deploy-all: deploy-worker deploy-site deploy-runner deploy-vcs-store deploy-archive deploy-differ

k8s:
	$(MAKE) -C k8s

check:
	$(MAKE) -C k8s check

.PHONY: k8s

%_pb2.py: %.proto
	protoc --python_out=. --mypy_out=. $<
