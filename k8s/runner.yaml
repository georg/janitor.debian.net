---
apiVersion: apps/v1
kind: Deployment
metadata:
 name: runner
spec:
 strategy:
   rollingUpdate:
     maxSurge: 1
     maxUnavailable: 1
   type: RollingUpdate
 replicas: 1
 selector:
   matchLabels:
     app: runner
 template:
   metadata:
     labels:
       app: runner
   spec:
     tolerations:
       - key: cloud.google.com/gke-preemptible
         operator: Equal
         value: "true"
         effect: NoSchedule
     containers:
       - name: runner
         image: ghcr.io/jelmer/janitor/runner:latest
         imagePullPolicy: Always
         livenessProbe:
           httpGet:
             path: /health
             port: 9911
           initialDelaySeconds: 3
           periodSeconds: 3
         args:
          - "--config=/etc/janitor/janitor.conf"
          - "--policy=/etc/janitor/policy.conf"
          - "--gcp-logging"
          - "--vcs-store-url=git=http://vcs-store-git.default:9923/git/,bzr=https://vcs-store-bzr.default:9929/bzr/"
         resources:
           limits:
             cpu: "1"
             memory: "4Gi"
           requests:
             cpu: "0.1"
             memory: "200M"
         ports:
           - containerPort: 9911
         volumeMounts:
           - name: janitor-config-volume
             mountPath: /etc/janitor/
           - name: breezy-config-volume
             mountPath: /root/.config/breezy/breezy.conf
             subPath: breezy.conf
           - name: breezy-config-volume
             mountPath: /root/.config/breezy/authentication.conf
             subPath: authentication.conf
           - name: breezy-secrets-volume
             readOnly: true
             mountPath: /root/.config/breezy/gitlab.conf
             subPath: gitlab.conf
           - name: breezy-secrets-volume
             readOnly: true
             mountPath: /root/.config/breezy/github.conf
             subPath: github.conf
           - name: ssh-key-volume
             readOnly: true
             mountPath: /root/.ssh/id_rsa
             subPath: id_rsa
           - name: ssh-conf-volume
             mountPath: /root/.ssh/config
           - name: keyring-config-volume
             readOnly: true
             mountPath: /root/.config/python_keyring
           - name: keyring-secrets-volume
             mountPath: /root/.local/share/python_keyring
         env:
          - name: PGHOST
            valueFrom:
              secretKeyRef:
                name: janitor-pguser-janitor
                key: pgbouncer-host
          - name: PGPORT
            valueFrom:
              secretKeyRef:
                name: janitor-pguser-janitor
                key: pgbouncer-port
          - name: PGDATABASE
            valueFrom:
              secretKeyRef:
                name: janitor-pguser-janitor
                key: dbname
          - name: PGUSER
            valueFrom:
              secretKeyRef:
                name: janitor-pguser-janitor
                key: user
          - name: PGPASSWORD
            valueFrom:
              secretKeyRef:
                name: janitor-pguser-janitor
                key: password
     volumes:
        - name: janitor-config-volume
          configMap:
            defaultMode: 420
            name: janitor-conf
        - name: keyring-config-volume
          configMap:
            defaultMode: 420
            name: keyring-conf
        - name: breezy-config-volume
          configMap:
            name: breezy-conf
            items:
              - key: breezy.conf
                path: breezy.conf
              - key: authentication.conf
                path: authentication.conf
            defaultMode: 256
        - name: breezy-secrets-volume
          secret:
            secretName: breezy-secrets
            items:
              - key: github.conf
                path: github.conf
              - key: gitlab.conf
                path: gitlab.conf
        - name: ssh-key-volume
          secret:
            secretName: ssh-key
            defaultMode: 256
        - name: ssh-conf-volume
          configMap:
            name: ssh-conf
            items:
              - key: config
                path: config
            defaultMode: 256
        - name: keyring-secrets-volume
          secret:
            secretName: keyring-secrets
---
apiVersion: v1
kind: Service
metadata:
  name: runner
  labels:
    app: runner
spec:
  ports:
    - port: 9911
      name: web
  selector:
    app: runner
  type: ClusterIP
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: runner
  labels:
    app: runner
spec:
  selector:
    matchLabels:
      app: runner
  endpoints:
    - port: web
