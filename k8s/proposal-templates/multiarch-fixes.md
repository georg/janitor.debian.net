{# vim: ft=jinja
 #}
{% extends "base.md" %}
{% block runner %}
Apply hints suggested by the multi-arch hinter.

{% for entry in applied %}
{% set kind = entry.link.split("#")[-1] %}
* {{ entry.binary }}: {% if entry.action %}{{ entry.action }}. This fixes: {{ entry.description }}. ([{{ kind }}]({{ entry.link }})){% else %}Fix: {{ entry.description }}. ([{{ kind }}]({{ entry.link }})){% endif %}
{% endfor %}

These changes were suggested on https://wiki.debian.org/MultiArch/Hints.

Note that in some cases, these multi-arch hints may trigger lintian warnings
until the dependencies of the package support multi-arch. This is expected,
see [/multiarch-fixes#why-does-lintian-warn](the FAQ) for details.

{% endblock %}
