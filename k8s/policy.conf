# Syntax:
# This file is read in order; later entries that match a package win.

# Each "policy" stanza can contain a number of settings and a set of
# matches that determine what packages the policy applies to.
# All conditions in a match must be met. A match can check for:
#
# * uploader: E-mail appears in Uploaders field.
# * maintainer: E-mail matches Maintainer field email.
# * source_package: Name matches Source package name
# * in_base: Does the package appear in the base distribution
# * before_stage: whether a release stage has not yet been entered

# Support modes:
#  build_only: Build the package, but don't do anything with it
#  push: Push the changes to the packaging branch
#  propose: Create a merge proposal with the changes
#  bts: Create and maintain a patch in the BTS
#  attempt_push: Push the changes to the packaging branch;
#     if the janitor doesn't have permission to push, create a merge proposal
#     instead.

# URL to load freeze information from.
freeze_dates_url: "https://release.debian.org/testing/freeze-and-release-dates.yaml"

# Default behaviour
policy {
  # Note that there's no match stanza here so policy here applies to all
  # packages.

  # Fixing lintian issues
  campaign {
    name: "lintian-fixes"
    command: "lintian-brush"
    publish { mode: propose }
    qa_review: required;
  }

  # Don't propose merges for upstream merges for now, since the janitor only
  # proposes changes to the main branch, not to pristine-tar/upstream.
  # See https://salsa.debian.org/jelmer/debian-janitor/issues/18
  campaign {
    name: "fresh-releases"
    command: "deb-new-upstream --refresh-patches"
    publish { mode: build_only }
    qa_review: required;
  }

  campaign {
    name: "fresh-snapshots"
    publish { mode: build_only }
    command: "deb-new-upstream --snapshot --refresh-patches"
    qa_review: required;
  }

  # Builds of packaging repositories without any changes applied.
  # These are used as base when generating debdiffs and diffoscope diffs.
  campaign {
    name: "unchanged"
    publish { mode: build_only }
    command: "true"
    qa_review: required;
  }

  # Apply multi-arch changes (apply-multiarch-hints)
  campaign {
    name: "multiarch-fixes"
    publish { mode: propose }
    command: "apply-multiarch-hints"
    qa_review: not_required;
  }

  # Mark orphaned packages as orphaned, updating Vcs-Git headers and Maintainer
  # field.
  campaign {
    name: "orphan"
    publish { mode: attempt_push }
    command: "deb-move-orphaned --no-update-vcs"
    qa_review: required;
  }

  # Drop people who are MIA from uploader fields
  campaign {
    name: "mia"
    publish { mode: attempt_push }
    command: "drop-mia-uploaders"
    qa_review: required;
  }

  # Import NMU changes
  campaign {
    name: "uncommitted"
    publish { mode: build_only }
    command: "deb-import-uncommitted"
    qa_review: required;
  }

  # Remove obsolete dependencies and other settings.
  campaign {
    name: "scrub-obsolete"
    publish { mode: propose }
    command: "deb-scrub-obsolete"
    qa_review: required;
  }

  # Generating Debian packaging from scratch
  campaign {
    name: "debianize"
    publish { mode: build_only }
    command: "debianize"
    qa_review: required;
  }

  campaign {
    name: "upstream-unchanged"
    publish { mode: build_only }
    command: "true"
    qa_review: required;
  }

  campaign {
    name: "bullseye-backports"
    publish { mode: build_only }
    command: "deb-auto-backport --target-release=bullseye"
    qa_review: required;
  }

  campaign {
    name: "buster-backports"
    publish { mode: build_only }
    command: "deb-auto-backport --target-release=buster"
    qa_review: required;
  }

  # Possible changelog types: auto, update, leave
  #
  # Auto means that the changelog will be updated by default, unless
  # some indicator is found that gbp-dch is used with the package
  # (e.g. a [dch] section in debian/gbp.conf)
  env {
    name: "DEB_UPDATE_CHANGELOG"
    value: "auto"
  }
}

policy {
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/debian\/.*" }
  # Mark orphaned packages as orphaned, updating Vcs-Git headers and Maintainer
  # field.
  campaign {
    name: "orphan"
    publish { mode: attempt_push }
  }
}

# Make sure we don't accidentally push to unsuspecting
# collab-maint repositories, even if debian-janitor becomes a
# member of "debian" in the future.
policy {
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/debian\/.*" }

  campaign {
    name: "multiarch-fixes"
    publish { mode: propose }
  }

  campaign {
    name: "lintian-fixes"
    publish { mode: propose }
  }
}

# Don't upgrade exabgp packages beyond oldstable.
policy {
  match { source_package: "exabgp" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=oldstable"
  }
}

# Don't upgrade pypy packages beyond anything still in Ubuntu ESM.
policy {
  match { source_package: "pypy" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=ubuntu/esm"
  }
}

# Packages that need to be backportable to Ubuntu LTS.
policy {
  match { maintainer: "pkg-grass-devel@lists.alioth.debian.org" }
  match { uploader: "sebastic@debian.org" }

  # Oldest supported LTS on Ubuntu.
  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=xenial"
    publish { mode: propose }
  }

  campaign {
    name: "scrub-obsolete"
    command: "deb-scrub-obsolete --compat-release=xenial --upgrade-release=xenial"
    publish { mode: propose }
  }
}

# People who prefer merge proposals without changelog updates,
# and for whom the autodetection doesn't work.
# See https://salsa.debian.org/jelmer/debian-janitor/issues/93
policy {
  match { maintainer: "hertzog@debian.org" }
  match { uploader: "hertzog@debian.org" }
  match { uploader: "olebole@debian.org" }
  match { source_package: "rdma-core" }
  match { maintainer: "pkg-gnome-maintainers@lists.alioth.debian.org" }
  match { maintainer: "pkg-go-maintainers@lists.alioth.debian.org" }
  match { maintainer: "paride@debian.org" }
  match { uploader: "paride@debian.org" }
  env {
     name: "DEB_UPDATE_CHANGELOG"
     value: "leave"
  }
}

# The GNUstep maintainers don't like debhelper-compat.
policy {
  match { maintainer: "pkg-gnustep-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=uses-debhelper-compat-file"
  }
}

# Rust uses debcargo
# See also https://salsa.debian.org/rust-team/debcargo/-/issues/39
policy {
  match { vcs_url_regex: "https://salsa.debian.org/rust-team/debcargo-conf/.*" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=upstream-metadata-file"
  }
}

# Romain prefers to apply public-upstream-key-not-minimal manually.
policy {
  match { uploader: "rfrancoise@debian.org" }
  match { maintainer: "rfrancoise@debian.org" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=public-upstream-key-not-minimal"
  }
}

# For packages that are maintained by the QA team, attempt to push
# but fall back to proposing changes.
policy {
  match {
    maintainer: "packages@qa.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: propose }
    publish { role: 'main' mode: propose }
    publish { role: 'pristine-tar' mode: propose }
    publish { role: 'upstream' mode: propose }
  }
}

policy {
  match { maintainer: "pkg-perl-maintainers@lists.alioth.debian.org" }
  env {
     name: "DEB_UPDATE_CHANGELOG"
     value: "update"
  }
}

# Enable attempt-push for the Perl team.
# See https://lists.debian.org/debian-perl/2019/12/msg00026.html
policy {
  match { maintainer: "pkg-perl-maintainers@lists.alioth.debian.org" }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push}
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the Hurd and Compiz teams.
policy {
  match { maintainer: "debian-hurd@lists.debian.org" }
  match { maintainer: "bugs@hypra.fr" }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push}
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the Go team.
policy {
  match { maintainer: "pkg-go-maintainers@lists.alioth.debian.org" }

  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push}
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the JS team. See
# https://alioth-lists.debian.net/pipermail/pkg-javascript-devel/2019-December/037607.html
policy {
  match {
    maintainer: "pkg-javascript-devel@lists.alioth.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }

  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: propose }
    publish { role: 'main' mode: propose }
    publish { role: 'pristine-tar' mode: propose }
    publish { role: 'upstream' mode: propose }
  }
}

# Enable attempt-push for the Ruby team.
# See https://lists.debian.org/debian-ruby/2019/12/msg00009.html
policy {
  match {
    maintainer: "pkg-ruby-extras-maintainers@lists.alioth.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the libreoffice team.
policy {
  match {
    maintainer: "debian-openoffice@lists.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
}

# Enable attempt-push for the pkg-security team.
# See https://lists.debian.org/debian-security-tools/2020/04/msg00048.html
policy {
  match {
    maintainer: "team+pkg-security@tracker.debian.org"
  }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "git-build-recipe" }

  # Oldest supported LTS on Ubuntu.
  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=xenial"
  }
}

# Enable attempt-push for some individuals
policy {
  match { maintainer: "jelmer@debian.org" }
  match { maintainer: "johfel@debian.org" }
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
    qa_review: not_required;
    broken_notify: email;
  }

  campaign {
    name: "fresh-releases"
    publish { role: 'main' mode: attempt_push }
    publish { role: 'pristine-tar' mode: attempt_push }
    publish { role: 'upstream' mode: propose }
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Requires contributor agreement
policy {
  match { source_package: "zeroc-ice" }
  match { source_package: "ice-builder-gradle" }
  match { vcs_url_regex: "https:\/\/github.com\/google\/.*" }

  campaign {
    name: "lintian-fixes"
    publish { mode: build_only }
  }

  campaign {
    name: "multiarch-fixes"
    publish { mode: build_only }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: build_only }
  }

  campaign {
    name: "fresh-snapshots"
    publish { mode: build_only }
  }

  campaign {
    name: "mia"
    publish { mode: build_only }
  }

  campaign {
    name: "orphan"
    publish { mode: build_only }
  }
}

# Opt out
policy {
  match { maintainer: "pali.rohar@gmail.com" }
  match { maintainer: "pkg-grass-devel@lists.alioth.debian.org" }
  # https://lists.debian.org/debian-devel/2020/04/msg00201.html
  match { maintainer: "debian-med-packaging@lists.alioth.debian.org" }
  match { vcs_url_regex: "https:\/\/salsa\.debian\.org\/med-team\/.*" }
  # https://lists.debian.org/debian-devel/2020/04/msg00205.html
  match { maintainer: "r-pkg-team@alioth-lists.debian.net" }
  match { maintainer: "abe@debian.org" }
  campaign {
    name: "lintian-fixes"
    publish { mode: build_only }
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: build_only }
  }
  campaign {
    name: "fresh-snapshots"
    publish { mode: build_only }
  }
  campaign {
    name: "fresh-releases"
    publish { mode: build_only }
  }
  campaign {
    name: "mia"
    publish { mode: build_only }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: build_only }
  }
}

# lintian-brush wraps *a lot* of lines in the changelog that are just over the
# limit.
policy {
  match { source_package: "debian-keyring" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=debian-changelog-line-too-long"
  }
}

# See https://salsa.debian.org/md/libretls/-/merge_requests/1#note_303640
policy {
  match { maintainer: "md@linux.it" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=trailing-whitespace"
  }
}

# policy for picca@debian.org
policy {
  match { maintainer: "picca@debian.org" }
  match { uploader: "picca@debian.org" }

  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: attempt_push }
  }

  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
  }
  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "debian-security-support" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=jessie"
  }
}

# No metadata file changes for the java maintainers
# https://salsa.debian.org/java-team/carrotsearch-hppc/-/merge_requests/1#note_165639
policy {
  match { maintainer: "pkg-java-maintainers@lists.alioth.debian.org" }
  # statsvn lives in java-team/, but is actually orphaned.
  match { source_package: "statsvn" }
  # epubcheck lives in java-team/, but is actually maintained by the Docbook
# XML/SGML maintainers.
  match { source_package: "epubcheck" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --exclude=upstream-metadata-file"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "lightgbm" }

  # Mark orphaned packages as orphaned, updating Vcs-Git headers and Maintainer
  # field.
  campaign {
    name: "orphan"
    publish { mode: propose }
  }

  # Import NMU changes
  campaign {
    name: "uncommitted"
    publish { mode: propose }
    command: "deb-import-uncommitted"
  }
}

# https://salsa.debian.org/webkit-team/webkit/-/merge_requests/5#note_176679
policy {
  match { source_package: "webkit2gtk" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=oldstable"
  }
}

# https://salsa.debian.org/debian/ddclient/-/merge_requests/1#note_177512
policy {
  match { source_package: "ddclient" }

  campaign {
    name: "lintian-fixes"
    command: "lintian-brush --compat-release=xenial --exclude=changelog-has-duplicate-line"
  }
}

# https://lists.debian.org/debian-python/2020/07/msg00112.html
# https://lists.debian.org/debian-python/2022/02/msg00046.html
policy {
  match { maintainer: "python-modules-team@lists.alioth.debian.org" }
  match { maintainer: "python-apps-team@lists.alioth.debian.org" }
  match { maintainer: "team+python@tracker.debian.org" }

  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }

  campaign {
    name: "multiarch-fixes"
    publish { mode: attempt_push }
  }

  campaign {
    name: "scrub-obsolete"
    publish { mode: attempt_push }
  }
}

# Access granted on 2020-09-10, by elbrus@
policy {
  match { maintainer: "team+debian-siridb-packaging-team@tracker.debian.org" }
  match { maintainer: "pkg-cacti-maint@lists.alioth.debian.org" }
  match { maintainer: "pkg-a11y-devel@lists.alioth.debian.org" }
  match { maintainer: "pkg-a11y-devel@alioth-lists.debian.net" }
  match { maintainer: "tts-project@lists.alioth.debian.org" }

  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
}

# packages in https://salsa.debian.org/debian/ that the janitor has explicitly
# been granted access to.
policy {
  match {
    source_package: "debhelper"
  }
  match {
    source_package: "mscgen"
  }
  match {
    source_package: "t1utils"
  }
  match {
    source_package: "apt-file"
  }
  match {
    source_package: "publicfile-installer"
  }

  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
}

policy {
  match { source_package: "breezy" }
  campaign {
    name: "lintian-fixes"
    publish { mode: bts }
    qa_review: not_required;
  }
  campaign {
    name: "multiarch-fixes"
    publish { mode: bts }
  }
}

policy {
  match { source_package: "libtimedate-perl" }
  campaign {
    name: "lintian-fixes"
    publish { mode: build_only }
  }
}

# The LLVM team backports to oldstable (?)
policy {
  match { maintainer: "pkg-llvm-team@lists.alioth.debian.org" }

  campaign {
   name: "lintian-fixes"
   command: "lintian-brush --compat-release=oldstable"
  }
}

# These packages are too large
policy {
  match { source_package: "git-annex" }
  match { source_package: "ansible" }
  match { source_package: "stellarium" }
  match { source_package: "freedict" }

  campaign {
    name: "lintian-fixes"
    publish { mode: skip }
  }

  campaign {
    name: "unchanged"
    publish { mode: skip }
  }

  campaign {
    name: "fresh-releases"
    publish { mode: skip }
  }

  campaign {
    name: "fresh-snapshots"
    publish { mode: skip }
  }
}

# explicitly opt in my packages, -- anarcat, 2021-08-26
policy {
  match { maintainer: "anarcat@debian.org" }
  # default policy is "propose", but I'm happy to grant access to certain repo after I see the merges work well in the future
  campaign {
    name: "lintian-fixes"
    publish { mode: attempt_push }
  }
  # default policy is "build_only", but I do want those new upstream releases, this is great
  campaign {
    name: "fresh-releases"
    command: "deb-new-upstream --refresh-patches"
    publish { mode: attempt_push }
  }
  # Import NMU changes as well
  campaign {
    name: "uncommitted"
    publish { mode: attempt_push }
  }
}
